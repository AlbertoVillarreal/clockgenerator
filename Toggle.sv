/********************************************************************
* Name:
*	Toogle.sv
*
* Description:
* 	This module do a toggle deoending of the clock
*
* Inputs:
*	oneShot						signal wich trigger the toggle
*	clk: 							board clock.
*	reset:						asynchronous reset.
* Outputs:
* 	clock_signal				1Hz clock
*
*
* Authors: 
* 	Miguel Agustin Gonzalez Rivera
*	Alberto Masaru Villarreal Morishige 
*
* Date: 
*	10/2/2018 
*
*********************************************************************/
module Toggle
(
   //Inputs
	input oneShot,
	input clk,
	input reset,
	
	//outputs
	output clkSignal
);
bit clkSignal_bit;

always_ff@(posedge clk or negedge reset)
begin
	if(reset == 1'b0)
		clkSignal_bit = 0;
	else
		begin
			if(oneShot == 1)
				clkSignal_bit = ~clkSignal_bit;
			else
				clkSignal_bit = clkSignal_bit;
		end
end
assign clkSignal = clkSignal_bit;
	
endmodule