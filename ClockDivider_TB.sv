timeunit 1ps; //It specifies the time unit that all the delay will take in the simulation.
timeprecision 1ps;// It specifies the resolution in the simulation.

module ClockDivider_TB;


 // Input Ports
bit clk = 0;
bit reset;
	
  // Output Ports
bit clock_signal;



ClockDivider
DUT
(
	.clk(clk),
	.reset(reset),
	.clock_signal(clock_signal)

);	


/*********************************************************/
initial // Clock generator
  begin
    forever #2 clk = !clk;
  end
/*********************************************************/
initial begin // reset generator
	#0 reset = 0;
	#5 reset = 1;
end
endmodule